/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//(function() {
//
//    var StripeBilling = {
//        init: function() {
//            this.form = $('#billing-form');
//            this.submitButton = this.form.find('input[type=submit]');
//            this.submitButtonValue=this.submitButton.val();
//            var stripeKey = $('meta[name="publishable-key"]').attr('content');
//
//            Stripe.setPublishableKey(stripeKey);
//            this.bindEvents();
//        },
//        bindEvents: function() {
//            this.form.on('submit', $.proxy(this.sendToken, this));
//        },
//        sendToken: function(event) {
//            this.submitButton.val('Proccessing ...').prop('disabled',true);
//            Stripe.createToken(this.form, $.proxy(this.stripeResponseHandler, this));
//            event.preventDefault();
//        },
//        stripeResponseHandler: function(status, response) {
//                
//            if (response.error) {
//                 this.form.find('.payment-errors').show().text(response.error.message);
//                 return this.submitButton.prop('disabled',false).val(this.submitButtonValue); 
//            }
//            $('<input>', {
//                type: 'hidden',
//                name: 'stripeToken',
//                value: response.id
//            }).appendTo(this.form);
////            this.form[0].submit();
////            this.form[0].preventDefault();
//
//        }
//    };
//    StripeBilling.init();
//})();





Stripe.setPublishableKey('pk_test_Q7dN1xo0ETIjKjk1Esi9uDxo');

jQuery(function($) {
  $('#billing-form').submit(function(event) {
    var $form = $(this);

    // Disable the submit button to prevent repeated clicks
    $form.find('submit').prop('disabled', true);

    Stripe.card.createToken({
      number: $('.card-number').val(),
      cvc: $('.card-cvc').val(),
      exp_month: $('.card-expiry-month').val(),
      exp_year: $('.card-expiry-year').val()
    }, stripeResponseHandler);

    // Prevent the form from submitting with the default action
    return false;
  });
});

function stripeResponseHandler(status, response) {
  var $form = $('#billing-form');

  if (response.error) {
    // Show the errors on the form
    $form.find('.payment-errors').text(response.error.message);
    $form.find('submit').prop('disabled', false);
  } else {
    // response contains id and card, which contains additional card details
    var token = response.id;
    // Insert the token into the form so it gets submitted to the server
    $form.append($('<input type="hidden" name="stripeToken" />').val(token));
    console.log(token);
    // and submit
    $form.get(0).submit();
  }
};