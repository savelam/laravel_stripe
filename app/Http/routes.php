<?php

/*
  |--------------------------------------------------------------------------
  | Routes File
  |--------------------------------------------------------------------------
  |
  | Here is where you will register all of the routes in an application.
  | It's a breeze. Simply tell Laravel the URIs it should respond to
  | and give it the controller to call when that URI is requested.
  |
 */

Route::get('/', function () {
    return view('welcome');
});

/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | This route group applies the "web" middleware group to every route
  | it contains. The "web" middleware group is defined in your HTTP
  | kernel and includes session state, CSRF protection, and more.
  |
 */
App::bind('App\Http\Billing\BillingInterface','App\Http\Billing\StripeBilling');
Route::group(['middleware' => ['web']], function () {
    //
    Route::get('/buy', function () {
        return view('buy');
    });
    Route::post('/buy', function () {
        $billing=App::make('App\Http\Billing\BillingInterface');
         $billing->charge([
            'email'=>  Illuminate\Support\Facades\Input::get('email'),
            'token'=>  Illuminate\Support\Facades\Input::get('stripeToken'),
        ]);
        
        return "Charge was successful";
//        dd(Input::all());
    });
});
