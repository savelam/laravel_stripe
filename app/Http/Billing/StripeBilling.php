<?php

namespace App\Http\Billing;
use Stripe;
use Stripe_Charge;
use Illuminate\Support\Facades\Config;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of StripeBilling
 *
 * @author dela
 */
class StripeBilling implements BillingInterface {

    //put your code here

    public function __construct() {
        
        \Stripe\Stripe::setApiKey(Config::get('stripe.secret_key'));
    }

    public function charge(array $data) {
        try {
            return \Stripe\Charge::create([
                        'amount' => 1000,
                        'currency' => 'usd',
                        'description' => $data['email'],
                        'card' => $data['token']
            ]);
        } catch (\Stripe\Error $ex) {
            // card was declined
            dd('card was declined ');
        }
    }

}
