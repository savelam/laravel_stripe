@extends('layouts.default')

@section('content')
<h1>Buy for $10</h1>
{!! Form::open(['id'=>'billing-form']) !!}
<div class="form-row">
    <label>
        <span>Card Number:</span>
        <input type="text" data-stripe='number' class="form-control card-number">
    </label>
</div>
<div class="form-row">
    <label>
        <span>CVC:</span>
        <input type="text" data-stripe='cvc' class="form-control card-cvc">
    </label>
</div>
<div class="form-row">
    <label>
        <span>Email:</span>
        <input type="text" data-stripe='email' class="form-control" name="email">
    </label>
</div>
<div class="form-row">
    <label>
        <span>Expiration Date:</span>
        {!! Form::selectMonth(null,null,['data-stripe'=>'exp-month','class' => 'card-expiry-month']) !!}
        {!! Form::selectYear(null,date('Y'),date('Y')+10, null,['data-stripe'=>'exp-year','class' => 'card-expiry-year']) !!}
    </label>
</div>
<div class="form-row">
    <label>
        {!! Form::submit('Buy Now') !!}
    </label>
</div>
<div class="payment-errors "></div>
{!! Form::close() !!}
@stop


@section('footer')
<!--<script>
    $(document).ready(function(){
       alert('jgh'); 
    });
</script>-->
<script src="/js/billing.js"></script>
@stop